main = {
    settings : null,
    canvasWindow : null,
    
    background : null,
    person : null,
    cabbage : null,
    wolf : null,
    goat : null,

    personX : 150,
    personY : 50,
    personW : 41,
    personH : 67,
    cabbageX : 200,
    cabbageY : 100,
    cabbageW : 40,
    cabbageH : 42,
    wolfX : 100,
    wolfY : 150,
    wolfW : 80,
    wolfH : 81,
    goatX : 200,
    goatY : 200,
    goatW : 114,
    goatH : 79,
    state1 : "CGTW",
    state2 : "Ø",

    init : function( pSettings, pCanvasWindow ) {
        main.settings = pSettings;
        main.canvasWindow = pCanvasWindow;

        window.addEventListener( "mousedown",   main.click, false );
        window.addEventListener( "mouseup",     main.unclick, false );
        window.addEventListener( "keydown",     main.keydown, false );
        window.addEventListener( "keyup",       main.keyup, false );
        window.addEventListener( "mousemove",   main.mousemove, false );

        main.loadGraphics();
    },

    loadGraphics : function() {
        main.background = new Image();
        main.person = new Image();
        main.cabbage = new Image();
        main.wolf = new Image();
        main.goat = new Image();

        main.background.src     = "images/bg.png";
        main.person.src         = "images/guy.png";
        main.cabbage.src        = "images/cabbage.png";
        main.wolf.src           = "images/wolf.png";
        main.goat.src           = "images/goat.png";
    },

    update : function() {
    },

    draw : function() {
        var bgColor = "#ffffff";
        
        // Fill background
        main.canvasWindow.fillStyle = bgColor;
        main.canvasWindow.fillRect( 0, 0, main.settings.width, main.settings.height );
        main.canvasWindow.drawImage( main.background, 0, 0 );

        main.canvasWindow.font = "15px Arial";
        main.canvasWindow.fillStyle = "#ffffff";
        main.canvasWindow.fillText( "Wolf / Goat / Cabbage game", 10, 15 );

        main.canvasWindow.font = "15px Arial";
        main.canvasWindow.fillStyle = "#34086c";
        main.canvasWindow.fillText( "Programmed by Rachel Singh", main.settings.width - 220, main.settings.height - 15 );

        main.canvasWindow.drawImage( main.person,   main.personX,   main.personY );
        main.canvasWindow.drawImage( main.cabbage,  main.cabbageX,  main.cabbageY );
        main.canvasWindow.drawImage( main.wolf,     main.wolfX,     main.wolfY );
        main.canvasWindow.drawImage( main.goat,     main.goatX,     main.goatY );

        // States
        main.canvasWindow.fillStyle = "#ffffff";
        main.canvasWindow.font = "20px Arial";
        main.canvasWindow.fillText( main.state1, 150, 350 );
        main.canvasWindow.fillText( main.state2, 620, 350 );
    },

    clear: function() {
    },

    isClicking: function( x, y, w, h, mouseX, mouseY ) {
        return (
            x <= mouseX &&
            mouseX <= x + w &&
            y <= mouseY &&
            mouseY <= y + h );
    },

    moveItem : function( mouseX, mouseY ) {
        main.state1 = "";
        main.state2 = "";
        
        if ( main.isClicking( main.personX, main.personY, main.personW, main.personH, mouseX, mouseY ) ) {
            if ( main.personX == 150 ) {
                main.personX = 550
            }
            else {
                main.personX = 150;
            }
        }
        
        if ( main.isClicking( main.cabbageX, main.cabbageY, main.cabbageW, main.cabbageH, mouseX, mouseY ) ) {
            if ( main.cabbageX == 200 ) {
                main.cabbageX = 600
            }
            else {
                main.cabbageX = 200;
            }
        }
        
        if ( main.isClicking( main.wolfX, main.wolfY, main.wolfW, main.wolfH, mouseX, mouseY ) ) {
            if ( main.wolfX == 100 ) {
                main.wolfX = 500
            }
            else {
                main.wolfX = 100;
            }
        }
        
        if ( main.isClicking( main.goatX, main.goatY, main.goatW, main.goatH, mouseX, mouseY ) ) {
            if ( main.goatX == 200 ) {
                main.goatX = 600
            }
            else {
                main.goatX = 200;
            }
        }

        if ( main.cabbageX == 200 ) { main.state1 += "C"; }
        else                        { main.state2 += "C"; }
        if ( main.goatX == 200 )    { main.state1 += "G"; }
        else                        { main.state2 += "G"; }
        if ( main.personX == 150 )  { main.state1 += "T"; }
        else                        { main.state2 += "T"; }
        if ( main.wolfX == 100 )    { main.state1 += "W"; }
        else                        { main.state2 += "W"; }

        if ( main.state1 == "" ) { main.state1 = "Ø"; }
        if ( main.state2 == "" ) { main.state2 = "Ø"; }
        
    },

    // Events
    click : function( event )
    {
        main.status = "";
        var windowRect = $( "canvas" )[0].getBoundingClientRect();
        var mouseX = event.clientX - windowRect.left;
        var mouseY = event.clientY - windowRect.top;

        main.moveItem( mouseX, mouseY );
    },

    unclick : function( event )
    {
        var windowRect = $( "canvas" )[0].getBoundingClientRect();
        var mouseX = event.clientX - windowRect.left;
        var mouseY = event.clientY - windowRect.top;
    },
    
    mousemove : function( event )
    {
        var windowRect = $( "canvas" )[0].getBoundingClientRect();
        var mouseX = event.clientX - windowRect.left;
        var mouseY = event.clientY - windowRect.top;
    },

    keydown : function( event )
    {
    },

    keyup : function( event )
    {
    }
};

