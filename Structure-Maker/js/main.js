main = {
    settings : null,
    canvasWindow : null,

    image : null,

    nodes : [],
    edges : [],

    sidebarButtons : {},
    buttons : {},
    
    mouseX : 0,
    mouseY : 0,

    status : "",

    lastClick : -1,
    
    mode : "node",
    labels : "num",
    helper : "",
    edgeDrag : -1,
    nodeDrag : -1,

    getDistance: function( x1, y1, x2, y2 ) {
        var dx = x2-x1;
        var dy = y2-y1;
        return Math.sqrt( dx*dx + dy*dy );
    },

    init : function( pSettings, pCanvasWindow ) {
        main.settings = pSettings;
        main.canvasWindow = pCanvasWindow;

        window.addEventListener( "mousedown",   main.click, false );
        window.addEventListener( "mouseup",     main.unclick, false );
        window.addEventListener( "keydown",     main.keydown, false );
        window.addEventListener( "keyup",       main.keyup, false );
        window.addEventListener( "mousemove",   main.mousemove, false );

        // Create buttons
        main.sidebarButtons["c"] = {
                x : 10,
                y : 150,
                width : 80,
                height : 40,
                big : "C / 0",
                label : "Clear graph",
                helper : "Graph cleared",
                background : "#ff00d2",
                selected : false
            };
            
        main.sidebarButtons["n"] = {
                x : 10,
                y : 200,
                width : 80,
                height : 40,
                big : "N / 1",
                label : "Draw node",
                helper : "Click on the drawing board to add a node; middle-click to remove it",
                background : "#718cff",
                selected : false
            };
            
        main.sidebarButtons["e"] = {
                x : 10,
                y : 250,
                width : 80,
                height : 40,
                big : "E / 2",
                label : "Draw edge",
                helper : "Click two nodes to add an edge between them; middle-click or choose new mode to stop.",
                background : "#00ffea",
                selected : false
            };
            
        main.sidebarButtons["m"] = {
                x : 10,
                y : 300,
                width : 80,
                height : 40,
                big : "M / 3",
                label : "Move node",
                helper : "Click and drag a node to relocate it",
                background : "#2aff00",
                selected : false
            };
            
        main.sidebarButtons["r"] = {
                x : 10,
                y : 350,
                width : 80,
                height : 40,
                big : "R / 4",
                label : "Rename node",
                helper : "Click on a node to rename it",
                background : "#ffde00",
                selected : false
            };
            
        main.sidebarButtons["z"] = {
                x : 10,
                y : 400,
                width : 80,
                height : 40,
                big : "Z / 5",
                label : "Erase node",
                helper : "Click on a node to remove it",
                background : "#ff6600",
                selected : false
            };
            
        main.sidebarButtons["x"] = {
                x : 10,
                y : 450,
                width : 80,
                height : 40,
                big : "X / 6",
                label : "Erase edge",
                helper : "Click on an edge to remove it",
                background : "#ff0000",
                selected : false
            };
            
        main.sidebarButtons["f"] = {
                x : 10,
                y : 500,
                width : 80,
                height : 40,
                big : "F / 7",
                label : "Fill array",
                helper : "Creating an array of items",
                background : "#aaaaaa",
                selected : false
            };

        var optionsStart = 580;

        main.buttons["num"] = {
                x : 10,
                y : optionsStart+50,
                width : 80,
                height : 40,
                big : "123",
                selected : true,
                textSize : "30px",
            };

        main.buttons["alpha"] = {
                x : 100,
                y : optionsStart+50,
                width : 80,
                height : 40,
                big : "ABC",
                selected : false,
                textSize : "30px",
        };

        main.selectMode( "n" );
    },

    update : function() {
    },

    draw : function() {
        var bgColor = "#ffffff";
        var nodeColor = "#34086c";
        var edgeLabelColor = nodeColor;
        
        // Fill background
        main.canvasWindow.fillStyle = bgColor;
        main.canvasWindow.fillRect( 0, 0, main.settings.width, main.settings.height );

        // Draw sidebar
        main.canvasWindow.fillStyle = "#2fa629";
        main.canvasWindow.fillRect( 0, 0, 300, main.settings.height );
        
        main.canvasWindow.fillStyle = "#14720f";
        main.canvasWindow.fillRect( 0, 40, 300, 20 );

        main.canvasWindow.fillStyle = "#ffffff";
        main.canvasWindow.font = "20px Arial";
        main.canvasWindow.fillText( "Data Structures Thingading", 10, 20 );
        
        main.canvasWindow.font = "30px Arial";
        main.canvasWindow.fillStyle = "#ffffff";
        main.canvasWindow.fillText( "MODES", 10, 120 );

        main.canvasWindow.font = "20px Arial";
        main.canvasWindow.fillText( "Draw mode: " + main.mode, 10, 57 );
        main.canvasWindow.fillText( main.status, 320, 680 );
        
        main.canvasWindow.font = "15px Arial";
        main.canvasWindow.fillText( "Programmed by Rachel Singh", 10, 710 );
    
        main.canvasWindow.font = "20px Arial";
        main.canvasWindow.fillStyle = "#000000";
        main.canvasWindow.fillText( main.helper, 320, 700 );

        // Helper text

        // Other options
        
        var optionsStart = 590;
        
        main.canvasWindow.font = "30px Arial";
        main.canvasWindow.fillStyle = "#ffffff";
        main.canvasWindow.fillText( "OPTIONS", 10, optionsStart );
        
        main.canvasWindow.font = "20px Arial";
        main.canvasWindow.fillStyle = "#ffffff";
        main.canvasWindow.fillText( "Node labels", 10, optionsStart+30 );

        
        // MODES buttons
        for ( var i in main.sidebarButtons )
        {
            if ( main.sidebarButtons[i].selected )
            {
                //console.log( main.sidebarButtons[i] );
            }

            if ( main.sidebarButtons[i].selected )
            {                
                main.canvasWindow.fillStyle = "#ffffff";
            }
            else
            {
                main.canvasWindow.fillStyle = main.sidebarButtons[i].background;
            }
                
            main.canvasWindow.fillRect(
                main.sidebarButtons[i].x,
                main.sidebarButtons[i].y,
                main.sidebarButtons[i].width,
                main.sidebarButtons[i].height );

            /*
            main.canvasWindow.beginPath();
            main.canvasWindow.strokeStyle = "000000";
            main.canvasWindow.rect(
                main.sidebarButtons[i].x,
                main.sidebarButtons[i].y,
                main.sidebarButtons[i].width,
                main.sidebarButtons[i].height );
            main.canvasWindow.stroke();
            * */
            
            main.canvasWindow.font = "30px Arial";
            main.canvasWindow.fillStyle = "#333333";
            main.canvasWindow.fillText( main.sidebarButtons[i].big, main.sidebarButtons[i].x+8, main.sidebarButtons[i].y+30 );
            
            main.canvasWindow.font = "20px Arial";
            main.canvasWindow.fillText( main.sidebarButtons[i].label, main.sidebarButtons[i].x+100, main.sidebarButtons[i].y+25 );
        }

        // More options
        for ( var i in main.buttons )
        {
            if ( main.buttons[i].selected )
                main.canvasWindow.fillStyle = "#ffff00";
            else
                main.canvasWindow.fillStyle = "#cccccc";
                
            main.canvasWindow.fillRect(
                main.buttons[i].x,
                main.buttons[i].y,
                main.buttons[i].width,
                main.buttons[i].height );

            main.canvasWindow.beginPath();
            main.canvasWindow.strokeStyle = "#000000";
            main.canvasWindow.rect(
                main.buttons[i].x,
                main.buttons[i].y,
                main.buttons[i].width,
                main.buttons[i].height );
            main.canvasWindow.stroke();
            
            main.canvasWindow.font = main.buttons[i].textSize + " Arial";
            main.canvasWindow.fillStyle = "#333333";
            main.canvasWindow.fillText( main.buttons[i].big, main.buttons[i].x+8, main.buttons[i].y+30 );
        }
        
        // Draw edges
        for ( var i = 0; i < main.edges.length; i++ )
        {
            main.canvasWindow.beginPath();
            if ( main.edges[i].hover && main.mode == "cut edge" )
                main.canvasWindow.strokeStyle = "#ff0000";
            else if ( main.edges[i].dragging )
                main.canvasWindow.strokeStyle = "#00ffea";
            else
                main.canvasWindow.strokeStyle = "#666666";
            main.canvasWindow.lineWidth = 5;
            
            if ( main.edges[i].endpointA == main.edges[i].endpointB )
            {
                // It's a loop
                main.canvasWindow.arc( main.edges[i].startX-20, main.edges[i].startY-20, 20, 2 * Math.PI, false );
                main.canvasWindow.stroke();
            }
            else
            {
                if ( main.edges[i].dragging == true )
                {
                    main.canvasWindow.moveTo( main.edges[i].startX, main.edges[i].startY );
                    main.canvasWindow.lineTo( main.mouseX, main.mouseY );
                }
                else
                {
                    main.canvasWindow.moveTo( main.edges[i].startX, main.edges[i].startY );
                    main.canvasWindow.lineTo( main.edges[i].endX, main.edges[i].endY );
                }
                main.canvasWindow.stroke();
            }
        }

        // Draw nodes
        for ( var i = 0; i < main.nodes.length; i++ )
        {
            main.canvasWindow.beginPath();
            if ( main.nodes[i].hover && main.mode == "cut node" )
            {
                main.canvasWindow.strokeStyle = "#ff0000";
                main.canvasWindow.fillStyle = "#ff6600";
            }
            else if ( main.nodes[i].dragging )
            {
                main.canvasWindow.strokeStyle = "#00ff00";
                main.canvasWindow.fillStyle = "#aaffaa";
            }
            else if ( main.nodes[i].hover && main.mode == "rename node" )
            {
                main.canvasWindow.strokeStyle = "#0000ff";
                main.canvasWindow.fillStyle = "#ffde00";
            }
            else if ( main.nodes[i].hover && main.mode == "move" )
            {
                main.canvasWindow.strokeStyle = "#0000ff";
                main.canvasWindow.fillStyle = "#2aff00";
            }
            else
            {
                main.canvasWindow.strokeStyle = "#000000";
                main.canvasWindow.fillStyle = "#ffffff";
            }
            
            //main.canvasWindow.arc( main.nodes[i].x, main.nodes[i].y, main.nodes[i].radius, 2 * Math.PI, false );
            main.canvasWindow.rect( main.nodes[i].x - main.nodes[i].width/2, main.nodes[i].y - main.nodes[i].height/2, main.nodes[i].width, main.nodes[i].height );
            main.canvasWindow.lineWidth = 2;
            main.canvasWindow.fill();
            main.canvasWindow.stroke();
            
            //main.canvasWindow.fillRect( main.nodes[i].x, main.nodes[i].y, 5, 5 );
            main.canvasWindow.font = "17px Arial";
            main.canvasWindow.fillStyle = "#000000";
            //main.canvasWindow.fillText( main.nodes[i].name, main.nodes[i].x-main.nodes[i].radius/3, main.nodes[i].y+main.nodes[i].radius/3 );
            main.canvasWindow.fillText( main.nodes[i].name, main.nodes[i].x - 3, main.nodes[i].y + 6 );
        }
        
        // Draw mouse hover
        if ( main.mode == "node" )
        {
            main.canvasWindow.font = "13px Arial";
            main.canvasWindow.fillStyle = "#333333";
            main.canvasWindow.fillText( "new node", main.mouseX - 30, main.mouseY - 15 );
            
            main.canvasWindow.strokeStyle = "#000000";
            main.canvasWindow.fillStyle = "#718cff";
            
            main.canvasWindow.beginPath();
            main.canvasWindow.arc( main.mouseX, main.mouseY, 5, 2 * Math.PI, false );
            main.canvasWindow.lineWidth = 1;
            main.canvasWindow.fill();
            main.canvasWindow.stroke();
        }
        else if ( main.mode == "edge" || main.mode == "new edge" )
        {
            main.canvasWindow.font = "13px Arial";
            main.canvasWindow.fillStyle = "#333333";
            main.canvasWindow.fillText( "new edge", main.mouseX - 25, main.mouseY - 15 );
            
            main.canvasWindow.beginPath();
            main.canvasWindow.strokeStyle = "#666666";
            main.canvasWindow.fillStyle = "#00ffea";
            
            main.canvasWindow.arc( main.mouseX, main.mouseY, 2, 2 * Math.PI, false );
            main.canvasWindow.lineWidth = 1;
            main.canvasWindow.fill();
            main.canvasWindow.stroke();
        }
        else if ( main.mode == "move" )
        {
            main.canvasWindow.font = "13px Arial";
            main.canvasWindow.fillStyle = "#333333";
            main.canvasWindow.fillText( "move", main.mouseX - 15, main.mouseY - 15 );
            
            main.canvasWindow.fillStyle = "#2aff00aa";
            
            main.canvasWindow.beginPath();
            main.canvasWindow.arc( main.mouseX, main.mouseY, 10, 2 * Math.PI, false );
            main.canvasWindow.lineWidth = 1;
            main.canvasWindow.fill();
        }
        else if ( main.mode == "rename node" )
        {
            main.canvasWindow.font = "13px Arial";
            main.canvasWindow.fillStyle = "#333333";
            main.canvasWindow.fillText( "rename", main.mouseX - 25, main.mouseY - 15 );
            
            main.canvasWindow.fillStyle = "#ffde00aa";
            
            main.canvasWindow.beginPath();
            main.canvasWindow.arc( main.mouseX, main.mouseY, 10, 2 * Math.PI, false );
            main.canvasWindow.lineWidth = 1;
            main.canvasWindow.fill();
        }
        else if ( main.mode == "cut node" )
        {
            main.canvasWindow.font = "13px Arial";
            main.canvasWindow.fillStyle = "#333333";
            main.canvasWindow.fillText( "x node", main.mouseX - 25, main.mouseY - 15 );
            
            main.canvasWindow.beginPath();
            main.canvasWindow.strokeStyle = "#ff6600aa";
            main.canvasWindow.moveTo( main.mouseX - 10, main.mouseY - 10 );
            main.canvasWindow.lineTo( main.mouseX + 10, main.mouseY + 10);
            main.canvasWindow.stroke();
            main.canvasWindow.beginPath();
            main.canvasWindow.moveTo( main.mouseX + 10, main.mouseY - 10 );
            main.canvasWindow.lineTo( main.mouseX - 10, main.mouseY + 10);
            main.canvasWindow.stroke();
        }
        else if ( main.mode == "cut edge" )
        {
            main.canvasWindow.font = "13px Arial";
            main.canvasWindow.fillStyle = "#333333";
            main.canvasWindow.fillText( "x edge", main.mouseX - 25, main.mouseY - 15 );
            
            main.canvasWindow.beginPath();
            main.canvasWindow.strokeStyle = "#ff0000aa";
            main.canvasWindow.moveTo( main.mouseX - 10, main.mouseY - 10 );
            main.canvasWindow.lineTo( main.mouseX + 10, main.mouseY + 10);
            main.canvasWindow.stroke();
            main.canvasWindow.beginPath();
            main.canvasWindow.moveTo( main.mouseX + 10, main.mouseY - 10 );
            main.canvasWindow.lineTo( main.mouseX - 10, main.mouseY + 10);
            main.canvasWindow.stroke();
        }
    },

    selectMode: function( mode ) {
        for ( var i in main.sidebarButtons )
        {
            main.sidebarButtons[i].selected = false;
        }

        main.sidebarButtons[ mode ].selected = true;
        
        if ( main.mode == "new edge" ) {
            main.stopNewEdge();
        }

        main.helper = main.sidebarButtons[ mode ].helper;
        
        if ( mode == "c" )
        {
            main.clear();
            main.selectMode( "n" );
        }
        else if ( mode == "n" )
        {
            main.mode = "node";
        }
        else if ( mode == "e" )
        {
            main.mode = "edge";
        }
        else if ( mode == "m" )
        {
            main.mode = "move";
        }
        else if ( mode == "x" )
        {
            main.mode = "cut edge";
        }
        else if ( mode == "z" )
        {
            main.mode = "cut node";
        }
        else if ( mode == "f" )
        {
            main.mode = "fill array";
        }
        else if ( mode == "d" )
        {
            main.debug();
        }
        else if ( mode == "r" )
        {
            main.mode = "rename node";
        }
    },

    selectOption: function( key )
    {
        // node labelling
        if ( key == "num" )
        {
            main.labels = "num";
            main.buttons["num"].selected = true;
            main.buttons["alpha"].selected = false;
        }
        else if ( key == "alpha" )
        {
            main.labels = "alpha";
            main.buttons["num"].selected = false;
            main.buttons["alpha"].selected = true;
        }
    },

    debug : function() {
        console.log( "NODES:" );
        for ( var i = 0; i < main.nodes.length; i++ )
        {
            console.log( main.nodes[i] );
        }

        console.log( "EDGES:" );
        for ( var i = 0; i < main.edges.length; i++ )
        {
            console.log( main.edges[i] );
        }
    },
    
    stopNewEdge : function() {
        main.edges.splice( main.edges.length-1, 1 );
        main.edgeDrag = -1;
        main.mode = "edge";
    },
    
    createVertex: function( newX, newY, label = "" ) {
        var newName = (main.nodes.length+1);
        if ( main.labels == "alpha" && label == "" )
        {
            newName = String.fromCharCode(main.nodes.length + 65);
        }
        else if ( label != "" )
        {
            newName = label;
        }
        main.nodes.push( { name : newName, x : newX, y : newY, width: 50, height: 25, radius : 15, hover : false, dragging : false } );
    },
    
    createEdge: function( x, y ) {
        // What node are we on?
        var start = -1;
        for ( var i = 0; i < main.nodes.length; i++ )
        {
            if ( main.isHovering( x, y, i ) )
            {
                start = i;
            }
        }

        if ( start == -1 )
        {
            // Need an end-point
            main.status = "Error: Must click a node as one end-point.";
            return;
        }
        
        main.edges.push( { startX : x, startY : y, endX : -1, endY : -1, hover : false, dragging : true, endpointA : start, endpointB : -1 } );
        main.mode = "new edge";
        main.edgeDrag = main.edges.length - 1;
    },

    attachEdge: function( x, y ) {
        // What node are we on?
        var start = -1;
        for ( var i = 0; i < main.nodes.length; i++ )
        {
            if ( main.isHovering( x, y, i ) )
            {
                start = i;
            }
        }

        if ( start == -1 )
        {
            // Need an end-point
            main.status = "Error: Must click a node as one end-point.";
            main.stopNewEdge();
            return;
        }

        main.edges[ main.edgeDrag ].dragging = false;
        main.edges[ main.edgeDrag ].endX = main.nodes[ start ].x;
        main.edges[ main.edgeDrag ].endY = main.nodes[ start ].y;
        main.edges[ main.edgeDrag ].endpointB = start;
        
        main.createEdge( x, y );

        //main.mode = "edge";
    },
    
    cutItem: function( x, y ) {
        var cutNode = -1;
        for ( var i = 0; i < main.nodes.length; i++ )
        {
            if ( main.isHovering( x, y, i ) )
            {
                cutNode = i;
                break;
            }
        }

        if ( cutNode >= 0 )
        {
            // Remove attached edges
            for ( var i = main.edges.length-1; i >= 0; i-- )
            {
                if ( main.edges[i].endpointA == cutNode || main.edges[i].endpointB == cutNode ) {
                    main.edges.splice( i, 1 );
                }
            }

            // Remove node
            main.nodes.splice( cutNode, 1 );
        }
    },

    cutEdge: function( x, y ) {
        var cutEdge = -1;
        for ( var i = 0; i < main.edges.length; i++ )
        {
            if ( main.isHoveringEdge( x, y, i ) )
            {
                cutEdge = i;
                break;
            }
        }

        if ( cutEdge >= 0 )
        {
            // Remove just the edge
            main.edges.splice( cutEdge, 1 );
        }
    },

    /*
     * 
        for ( var i = 0; i < main.edges.length; i++ )
        {
            if ( main.isHoveringEdge( x, y, i ) )
            {
                main.edges[i].hover = true;
            }
            else
            {
                main.edges[i].hover = false;
            }
        }
     * */

    renameVertex: function( x, y ) {
        var renameNode = -1;
        for ( var i = 0; i < main.nodes.length; i++ )
        {
            if ( main.isHovering( x, y, i ) )
            {
                renameNode = i;
            }
        }
        
        if ( renameNode >= 0 )
        {
            var newName = prompt( "Enter a new name" );
            main.nodes[ renameNode ].name = newName;
        }
    },

    beginMove: function( x, y ) {
        var active = -1;
        for ( var i = 0; i < main.nodes.length; i++ )
        {
            if ( main.isHovering( x, y, i ) )
            {
                active = i;
            }
        }

        main.nodeDrag = active;

        if ( active >= 0 )
        {
            main.nodes[ active ].dragging = true;
        }
    },

    moveNode: function( x, y ) {
        if ( main.nodeDrag >= 0 ) {
            main.nodes[ main.nodeDrag ].x = x;
            main.nodes[ main.nodeDrag ].y = y;

            // Move its attached edges
            for ( var i = 0; i < main.edges.length; i++ )
            {
                if ( main.edges[i].endpointA == main.nodeDrag ) {
                    main.edges[i].startX = x;
                    main.edges[i].startY = y;
                }
                
                if ( main.edges[i].endpointB == main.nodeDrag ) {
                    main.edges[i].endX = x;
                    main.edges[i].endY = y;
                }
            }
        }
    },

    endMove: function( x, y ) {
        console.log( "Done moving" );
        for ( var i = 0; i < main.nodes.length; i++ )
        {
            main.nodes[ i ].dragging = false;
        }

        main.nodeDrag = -1;
    },
    
    fillArray: function() {
      var x = 410, y = 300;
      main.createVertex( x + 000, y, "6" );
      main.createVertex( x + 100, y, "5" );
      main.createVertex( x + 200, y, "3" );
      main.createVertex( x + 300, y, "1" );
      main.createVertex( x + 400, y, "8" );
      main.createVertex( x + 500, y, "6" );
      main.createVertex( x + 600, y, "2" );
      main.createVertex( x + 700, y, "4" );
    },
    
    hoverItem: function( x, y ) {
        for ( var i = 0; i < main.nodes.length; i++ )
        {
            /*
            if ( x >= main.nodes[i].x - main.nodes[i].radius &&
                x <= main.nodes[i].x + main.nodes[i].radius &&
                y >= main.nodes[i].y - main.nodes[i].radius &&
                y <= main.nodes[i].y + main.nodes[i].radius )
                * */
            if ( x >= main.nodes[i].x - main.nodes[i].width/2 &&
                x <= main.nodes[i].x + main.nodes[i].width/2 &&
                y >= main.nodes[i].y - main.nodes[i].height/2 &&
                y <= main.nodes[i].y + main.nodes[i].height/2 )
            {
                main.nodes[i].hover = true;
            }
            else
            {
                main.nodes[i].hover = false;
            }
        }
    },

    hoverEdge: function( x, y ) {
        for ( var i = 0; i < main.edges.length; i++ )
        {
            if ( main.isHoveringEdge( x, y, i ) )
            {
                main.edges[i].hover = true;
            }
            else
            {
                main.edges[i].hover = false;
            }
        }
    },

    distance: function( x1, y1, x2, y2 ) {
        var dx = x2 - x1;
        var dy = y2 - y1;
        return Math.sqrt( dx*dx + dy*dy );
    },

    isHovering: function( x, y, nodeIndex ) {
        for ( var i = 0; i < main.nodes.length; i++ )
        {
          /*
            if ( i == nodeIndex &&
                x >= main.nodes[i].x - main.nodes[i].radius &&
                x <= main.nodes[i].x + main.nodes[i].radius &&
                y >= main.nodes[i].y - main.nodes[i].radius &&
                y <= main.nodes[i].y + main.nodes[i].radius )
                * */
            if ( i == nodeIndex &&
                x >= main.nodes[i].x - main.nodes[i].width/2 &&
                x <= main.nodes[i].x + main.nodes[i].width/2 &&
                y >= main.nodes[i].y - main.nodes[i].height/2 &&
                y <= main.nodes[i].y + main.nodes[i].height/2 )
            {
                return true;
            }
        }
        return false;
    },

    isHoveringEdge: function( x, y, edgeIndex ) {
        if ( main.edges.length == 0 ) { return false; }
        
        if ( main.edges[ edgeIndex ].endpointA == null || main.edges[ edgeIndex ].endpointB == null )
        {
            return false;
        }
        
        var startIndex = main.edges[ edgeIndex ].endpointA;
        var endIndex = main.edges[ edgeIndex ].endpointB;

        if ( startIndex == -1 || endIndex == -1 )
        {
            return false;
        }
        
        var startNode = main.nodes[ startIndex ];
        var endNode = main.nodes[ endIndex ];

        // What's the angle between two points?
        var angle = Math.atan2( endNode.y - startNode.y, endNode.x - startNode.x );

        // What's the angle between one point and the mouse?
        var angle2 = Math.atan2( y - startNode.y, x - startNode.x );

        var range = 0.10;

        if ( angle2 > angle - range && angle2 < angle + range )
        {
            // Hover
            return true;
        }
        else
        {
            return false;
        }
    },

    clear: function() {
        main.edges.splice( 0, main.edges.length );
        main.nodes.splice( 0, main.nodes.length );
    },

    isClickingButton: function( mouseX, mouseY, regionX1, regionY1, regionX2, regionY2 )
    {
        return ( mouseX > regionX1 && mouseX < regionX2 &&
                mouseY > regionY1 && mouseY < regionY2 );
    },

    // Events
    click : function( event )
    {
        var windowRect = $( "canvas" )[0].getBoundingClientRect();
        var mouseX = event.clientX - windowRect.left;
        var mouseY = event.clientY - windowRect.top;
        
        // Check quick buttons
        if ( mouseX <= 300 )
        {
            for ( var i in main.sidebarButtons )
            {
                if ( main.isClickingButton(
                    mouseX, mouseY,
                    main.sidebarButtons[i].x, main.sidebarButtons[i].y,
                    main.sidebarButtons[i].x + main.sidebarButtons[i].width,
                    main.sidebarButtons[i].y + main.sidebarButtons[i].height
                    ) )
                {
                    main.selectMode( i );
                }
            }

            for ( var i in main.buttons )
            {
                if ( main.isClickingButton(
                    mouseX, mouseY,
                    main.buttons[i].x, main.buttons[i].y,
                    main.buttons[i].x + main.buttons[i].width,
                    main.buttons[i].y + main.buttons[i].height
                    ) )
                {
                    main.selectOption( i );
                }
            }
            
            if ( main.mode == "fill array" )
            {
              main.fillArray();
            }
        }
        else
        {
        // Check graph
            main.status = "";

            var lastClick = main.lastClick;
            var newClick = lastClick;

            if ( event.button == 0 ) // left
            {
                if ( main.mode == "node" ) {
                    main.createVertex( mouseX, mouseY );
                }
                else if ( main.mode == "edge" ) {
                    main.createEdge( mouseX, mouseY );
                }
                else if ( main.mode == "new edge" ) {
                    main.attachEdge( mouseX, mouseY );
                }
                else if ( main.mode == "move" ) {
                    main.beginMove( mouseX, mouseY );
                }
                else if ( main.mode == "cut node" ) {
                    main.cutItem( mouseX, mouseY );
                }
                else if ( main.mode == "cut edge" ) {
                    main.cutEdge( mouseX, mouseY );
                }
                else if ( main.mode == "rename node" ) {
                    main.renameVertex( mouseX, mouseY );
                }
            }
            else if ( event.button == 1 ) // middle
            {
                if ( main.mode == "new edge" ) {
                    main.stopNewEdge();
                }
                main.cutItem( mouseX, mouseY );
            }
        }

    },

    unclick : function( event )
    {
        var windowRect = $( "canvas" )[0].getBoundingClientRect();
        var mouseX = event.clientX - windowRect.left;
        var mouseY = event.clientY - windowRect.top;
        
        if ( main.mode == "move" ) {
            main.endMove( mouseX, mouseY );
        }
    },
    
    mousemove : function( event )
    {
        var windowRect = $( "canvas" )[0].getBoundingClientRect();
        var mouseX = event.clientX - windowRect.left;
        var mouseY = event.clientY - windowRect.top;
        
        main.mouseX = mouseX;
        main.mouseY = mouseY;
        
        main.hoverItem( mouseX, mouseY );
        main.hoverEdge( mouseX, mouseY );

        if ( main.mode == "move" ) {
            main.moveNode( mouseX, mouseY );
        }
    },

    keydown : function( event )
    {
        if      ( event.key == "c" || event.key == "0" )     main.selectMode( "c" );
        else if ( event.key == "n" || event.key == "1" )     main.selectMode( "n" );
        else if ( event.key == "e" || event.key == "2" )     main.selectMode( "e" );
        else if ( event.key == "m" || event.key == "3" )     main.selectMode( "m" );
        else if ( event.key == "r" || event.key == "4" )     main.selectMode( "r" );
        else if ( event.key == "z" || event.key == "5" )     main.selectMode( "z" );
        else if ( event.key == "x" || event.key == "6" )     main.selectMode( "x" );
        else if ( event.key == "f" || event.key == "7" )     main.fillArray();
    },

    keyup : function( event )
    {
    }
};




